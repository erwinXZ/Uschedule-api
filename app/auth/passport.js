var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;

var ServicioUsuario = require('./servicio.js');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = '37LvDSm4XvjYOh9Y';

passport.use(new JwtStrategy(opts, function(jwtContent, done) {
  ServicioUsuario.getOne(jwtContent._id).then((user) => {
    return done(null, user);
  }).catch((err) => {
    return done(err, false);
  });
}));

var auth = {};
auth.isLoggedIn = passport.authenticate('jwt', { session: false });

module.exports = auth;
