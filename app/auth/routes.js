var express = require('express')  ;
var router = express.Router();
var Servicio = require('./servicio.js');
var auth = require('../auth/passport.js');
var apiHelper = require('../utils/apiHelper.js');

function login(req, res) {
  var p = Servicio.login(req.body.usuario, req.body.password);
  apiHelper.handle(res, p);
}

function add(req, res) {
  var p = Servicio.add(req.body);
  apiHelper.handle(res, p);
}

function me(req, res) {
  var p = Servicio.getOne(req.user._id);
  apiHelper.handle(res, p);
}

router.post('/login', login);
router.post('/users', add);
router.get('/me', auth.isLoggedIn, me);
module.exports = router;
