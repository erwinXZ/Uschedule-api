var Usuario = require('./usuario.model.js');
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var privateKey = '37LvDSm4XvjYOh9Y';
var jwt = require('jsonwebtoken');
var errorCodes = require('../utils/errorCodes.js');
var MongoRepo = require('../utils/mongoRepo.js');
var repo = new MongoRepo(Usuario);

// method to encrypt data(password)
function encrypt(password) {
  var cipher = crypto.createCipher(algorithm, privateKey);
  var crypted = cipher.update(password, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

function noExiste(nombreUsuario) {
  let query = {
    usuario: nombreUsuario
  };
  return repo.find(query).then(res => {
    if (res.length > 0) {
      throw {
        errorCode: errorCodes.CONFLICT
      };
    }
    return true;
  });
}

var servicio = {
  login: function(usuario, password) {
    return repo.find({
      usuario: usuario
    }).then(function(res) {
      if (res.length === 0) {
        throw {
          errorCode: errorCodes.NOT_AUTH,
          mensaje: 'Usuario o password incorrecto'
        };
      }
      var user = res[0];

      if (user.password !== password) {
        throw {
          errorCode: errorCodes.NOT_AUTH,
          mensaje: 'Usuario o password incorrecto'
        };
      }

      var userToSend = {
        usuario: user.usuario,
        _id: user._id
      };
      userToSend.token = jwt.sign(userToSend, privateKey);
      return userToSend;
    });
  },
  add: function(objecto) {
    return noExiste(objecto.usuario).then(() => {
      // objecto.password = encrypt(objecto.password);
      return repo.add(objecto).then(user => {
        var userToSend = {
          user: user.usuario,
          _id: user._id
        };
        userToSend.token = jwt.sign(userToSend, privateKey);
        return userToSend;
      });
    }).catch((err) => {
      throw err;
    });
  },
  getAll: function() {
    return repo.getAll();
  },
  getOne: function(id) {
    return repo.getOne(id);
  },
  remove: function(id) {
    return repo.remove(id);
  }
};

module.exports = servicio;
