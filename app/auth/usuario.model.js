var mongoose = require('mongoose');
mongoose.promise = Promise;
var UsuarioSchema = mongoose.Schema({
  user: {
    type: String,
    index: true,
  },
  password: String,
  role: String
});

var usuario = mongoose.model('Usuario', UsuarioSchema);
module.exports = usuario;
