var mongoose = require('mongoose');
var schemaSubject = mongoose.Schema({
  acronym: String,
  name: String,
  semester: Number,
  teachers: [{
    degree: String,
    name: String,
    phone: String,
    schedules:[{
      classroom: String,
      day: String,
      finishTime: String,
      initTime: String,
    }],
    assistantships: [{
      email: String,
      name: String,
      phone: String,
      schedules:[{
        classroom: String,
        day: String,
        finishTime: String,
        initTime: String,
      }]
    }]
  }]
},
{
  versionKey: false
}
);

var SubjectModel = mongoose.model('Subject', schemaSubject);
module.exports = SubjectModel;
