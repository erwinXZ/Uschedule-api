var express = require('express');
var Service = require('./service.js');
var apiHelper = require('../utils/apiHelper.js');
// var auth = require('../auth/passport');

function getAll(req, res) {
  let promise = Service.getAll();
  apiHelper.handle(res, promise);
}

function getOne(req, res) {
  console.log(req.params.id);
  let promise = Service.getOne(req.params.id);
  apiHelper.handle(res, promise);
}

function add(req, res) {
  let promise = Service.add(req.body);
  apiHelper.handle(res, promise);
}

function update(req, res) {
  var id = req.params.id;
  let promise = Service.update(id, req.body);
  apiHelper.handle(res, promise);
}

function remove(req, res) {
  var id = req.params.id;
  let promise = Service.remove(id);
  apiHelper.handle(res, promise);
}

var router = express.Router();

router.get('/subject/', getAll);
router.post('/addSubject/', add);
router.get('/getSubject/:id', getOne);
router.put('/updateSubject/:id', update);
router.delete('/deleteSubject/:id', remove);

module.exports = router;
