var errorCodes = require('../utils/errorCodes.js');
var model = require('./model.js');
var MongoRepo = require('../utils/mongoRepo.js');
var repo = new MongoRepo(model);

function checkExists(docs){
  if (docs.lenght === 0) {
    throw {
      errorCode: errorCodes.NOT_FOUND
    };
  }
  return docs;
}
var service = {
  getAll: function() {
    return repo.getAll();
  },
  getOne: function(id) {
    return repo.getOne(id)
      .then(checkExists);
  },
  add: function(task) {
    return repo.add(task);
  },
  update: function(id, task) {
    return this.getOne(id).then(() => {
      return repo.update(id, task);
    });
  },
  remove: function(id) {
    return this.getOne(id).then(() => {
      return repo.remove(id);
    });
  },
};

module.exports = service;
