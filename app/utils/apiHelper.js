let errorCodes = require('./errorCodes.js');
let debug = require('debug')('ERROR');

function internalError(res) {
  res.status(500);
  res.send('Internal server error');
}

let helper = {
  response: function(res) {
    return function(json) {
      res.json(json);
    };
  },
  error: function(res) {
    return function(e) {
      if (!e) {
        internalError(res);
      }
      switch (e.errorCode) {
      case errorCodes.NOT_FOUND:
        res.status(404);
        res.send('Not found');
        break;
      case errorCodes.CONFLICT:
        res.status(409);
        res.send('Conflict');
        break;
      case errorCodes.NOT_AUTH:
        res.status(401);
        res.send('NOT_AUTH');
        break;
      default:
        internalError(res);
        break;
      }
      debug(e);
    };
  },
  handle: function(res, promise) {
    promise.then(helper.response(res)).catch(helper.error(res));
  }
};

module.exports = helper;
