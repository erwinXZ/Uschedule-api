var fs = require('fs');
var path = require('path');
var errorCodes = require('./errorCodes.js');

function isNotDirectory(file) {
  return fs.statSync(file).isFile();
}

function fileToObject(file) {
  return new Promise((res, rej) => {
    fs.readFile(file, (err, data) => {
      if (err) {
        return rej(err);
      }
      res(JSON.parse(data));
    });
  });
}

class Repo {
  constructor(folderName) {
    this.folderName = folderName;
    if (!fs.existsSync(folderName)) {
      fs.mkdir(folderName);
    }
  }
  getFileName(id) {
    return path.join(this.folderName, id);
  }
  add(object) {
    object._id = Math.random().toString(16).substr(2, 8);
    let strObj = JSON.stringify(object);
    let fileName = this.getFileName(object._id);
    return new Promise((res, rej) => {
      fs.writeFile(fileName, strObj, (err) => {
        if (err) {
          err.errorCode = errorCodes.INTERNAL;
          return rej(err);
        }
        res(Object.assign({}, object));
      });
    });
  }
  getAll() {
    return new Promise((res, rej) => {
      fs.readdir(this.folderName, (err, files) => {
        if (err) {
          return rej(err);
        }
        let promises = files
          .map(this.getFileName.bind(this))
          .filter(isNotDirectory)
          .map(file => fileToObject(file));
        Promise.all(promises).then(results => res(results));
      });
    });
  }
  getOne(id) {
    let fileName = this.getFileName(id);
    if (fs.existsSync(fileName)) {
      return fileToObject(fileName);
    } else {
      return Promise.reject({
        errorCode: errorCodes.NOT_FOUND
      });
    }
  }

  update(id, object) {
    let strObj = JSON.stringify(object);
    let fileName = this.getFileName(id);
    return new Promise((res, rej) => {
      fs.writeFile(fileName, strObj, (err) => {
        if (err) {
          err.errorCode = errorCodes.INTERNAL;
          return rej(err);
        }
        res(Object.assign({}, object));
      });
    });
  }

  remove(id) {
    let fileName = this.getFileName(id);
    if (fs.existsSync(fileName)) {
      return fileToObject(fileName).then(object => {
        return new Promise((res, rej) => {
          fs.unlink(fileName, (err) => {
            if (err) {
              return rej(err);
            }
            res(object);
          });
        });
      });
    } else {
      return Promise.reject({
        errorCode: errorCodes.NOT_FOUND
      });
    }
  }
}

module.exports = Repo;
