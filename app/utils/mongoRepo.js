var errorCodes = require('./errorCodes.js');

function checkExists(obj) {
  if (!obj) {
    throw {
      errorCode: errorCodes.NOT_FOUND
    };
  }
  return obj;
}

class Repo {
  constructor(model) {
    this.model = model;
  }
  find(query) {
    return this.model.find(query).lean().exec();
  }
  add(object) {
    let newObject = new this.model(object);
    return newObject.save().then(obj => obj.toObject());
  }
  getAll() {
    return this.model.find({}).lean().exec();
  }
  getOne(id) {
    return this.model.findById(id).lean().exec().then(checkExists);
  }
  update(id, object) {
    let opt = {'new': true};
    return this.model.findByIdAndUpdate(id, object, opt).lean().exec().then(checkExists);
  }
  remove(id) {
    return this.model.findByIdAndRemove(id).lean().exec().then(checkExists);
  }
}

module.exports = Repo;
