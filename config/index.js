let config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3000,
  ip: process.env.IP,
  seedDB: false,
  mongoOptions: {
    useMongoClient: true
  }
};

module.exports = Object.assign(config,
  require('./' + config.env + '.js'),
  require('./shared.js'));
