module.exports = {
  port: process.env.PORT || 8080,
  seedBD: true,
  mongoUri: process.env.MONGOLAB_URI
            || process.env.MONGOHQ_URI
            || 'mongodb://localhost/schedule'
};
