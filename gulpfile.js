/* eslint-disable no-console */
var gulp = require('gulp');
var nodemon = require('nodemon');
var livereload = require('gulp-livereload');
var sequence = require('run-sequence');
var open = require('gulp-open');
var http = require('http');
var del = require('del');
var runSequence = require('run-sequence');

var localEnv = require('./config/local.env.js');

//gulp.task(nombre, [deps], function);
gulp.task('test', function() {
  console.log('ejecutar test');
});

function checkAppReady(cb) {
  var options = {
    host: 'localhost',
    port: 3000
  };
  http.get(options, () => cb(true))
    .on('error', () => cb(false));
}

// Call page until first success
function whenServerReady(cb) {
  var serverReady = false;
  var appReadyInterval = setInterval(() =>
    checkAppReady((ready) => {
      if (!ready || serverReady) {
        return;
      }
      clearInterval(appReadyInterval);
      serverReady = true;
      cb();
    }), 100);
}

gulp.task('browser:reload', function() {
  livereload.reload();
});

gulp.task('browser:open', function() {
  whenServerReady(() => {
    gulp.src(__filename)
      .pipe(open({
        uri: 'http://localhost:3000'
      }));
  });
});

gulp.task('start:server', function() {
  livereload.listen({
    port: 1234
  });
  nodemon({
    script: 'index.js',
    env: localEnv
  }).on('start', function() {
    setTimeout(function() {
      livereload.reload();
    }, 1000);
  });
});

gulp.task('serve', function() {
  sequence('start:server', 'browser:open');
});

gulp.task('default', ['serve']);
