var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var config = require('./config');
var router = require('./router.js');

// function intermedia(req, res, next) {
//   console.log(req.url);
//   console.log('******');
//   next();
// }

var app = express();
app.use(morgan('dev'));
// app.use(intermedia);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
  
      // Website you wish to allow to connect
      res.setHeader('Access-Control-Allow-Origin', '*');
  
      // Request methods you wish to allow
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
      // Request headers you wish to allow
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  
      // Set to true if you need the website to include cookies in the requests sent
      // to the API (e.g. in case you use sessions)
      res.setHeader('Access-Control-Allow-Credentials', true);
  
      // Pass to next layer of middleware
      next();
  });

router(app);

mongoose.connect(config.mongoUri, config.mongoOptions);

// if (config.seedDB) {
//   require('./config/seed.js');
// }

app.listen(config.port, config.ip, function() {
  // console.log('Servidor en puerto ' + config.port);
});
