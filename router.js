var path = require('path');
var express = require('express');

function register(app) {
  app.use('/', require('./app/subject/router.js'));
  app.use('/', require('./app/auth/routes.js'));
  app.use(express.static(path.join(__dirname, 'client')));
  app.get('/*', function(req, res) {
    res.redirect('/');
  });
}

module.exports = register;
